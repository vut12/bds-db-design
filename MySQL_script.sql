-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema train_transport_management
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema train_transport_management
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `train_transport_management` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`city` (
  `id` INT UNSIGNED NOT NULL,
  `city_name` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`station`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`station` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `station_name` VARCHAR(15) NOT NULL,
  `traffic` VARCHAR(10) NOT NULL,
  `city_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`, `city_id`),
  INDEX `fk_station_city1_idx` (`city_id` ASC) VISIBLE,
  CONSTRAINT `fk_station_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `mydb`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`depo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`depo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `depo_size` VARCHAR(10) NOT NULL,
  `depo_name` VARCHAR(10) NOT NULL,
  `station_id` INT NOT NULL,
  `city_id` INT UNSIGNED NOT NULL,
  `station_id1` INT NOT NULL,
  `station_city_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`, `station_id`, `city_id`, `station_id1`, `station_city_id`),
  INDEX `fk_depo_city1_idx` (`city_id` ASC) VISIBLE,
  INDEX `fk_depo_station1_idx` (`station_id1` ASC, `station_city_id` ASC) VISIBLE,
  CONSTRAINT `fk_depo_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `mydb`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_depo_station1`
    FOREIGN KEY (`station_id1` , `station_city_id`)
    REFERENCES `mydb`.`station` (`id` , `city_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`person` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `last_name` VARCHAR(15) NOT NULL,
  `age` INT NOT NULL,
  `gender` VARCHAR(10) NOT NULL,
  `working_years` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`train_controller`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`train_controller` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `person_id` INT NOT NULL,
  PRIMARY KEY (`id`, `person_id`),
  INDEX `fk_train_controller_person1_idx` (`person_id` ASC) VISIBLE,
  CONSTRAINT `fk_train_controller_person1`
    FOREIGN KEY (`person_id`)
    REFERENCES `mydb`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`train`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`train` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `train_name` VARCHAR(10) NOT NULL,
  `speed` VARCHAR(10) NOT NULL,
  `station_id` INT NOT NULL,
  `depo_id` INT NOT NULL,
  `depo_station_id` INT NOT NULL,
  `train_id` INT NOT NULL,
  `train_controller_id` INT NOT NULL,
  `train_controller_person_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_train_station1_idx` (`station_id` ASC) VISIBLE,
  INDEX `fk_train_depo1_idx` (`depo_id` ASC, `depo_station_id` ASC) VISIBLE,
  INDEX `fk_train_train1_idx` (`train_id` ASC) VISIBLE,
  INDEX `fk_train_train_controller1_idx` (`train_controller_id` ASC, `train_controller_person_id` ASC) VISIBLE,
  CONSTRAINT `fk_train_station1`
    FOREIGN KEY (`station_id`)
    REFERENCES `mydb`.`station` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_train_depo1`
    FOREIGN KEY (`depo_id` , `depo_station_id`)
    REFERENCES `mydb`.`depo` (`id` , `station_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_train_train1`
    FOREIGN KEY (`train_id`)
    REFERENCES `mydb`.`train` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_train_train_controller1`
    FOREIGN KEY (`train_controller_id` , `train_controller_person_id`)
    REFERENCES `mydb`.`train_controller` (`id` , `person_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`locomotive`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`locomotive` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `locomotive_colour` VARCHAR(10) NOT NULL,
  `type` VARCHAR(10) NOT NULL,
  `depo_id` INT NOT NULL,
  `depo_station_id` INT NOT NULL,
  `train_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_locomotive_depo1_idx` (`depo_id` ASC, `depo_station_id` ASC) VISIBLE,
  INDEX `fk_locomotive_train1_idx` (`train_id` ASC) VISIBLE,
  CONSTRAINT `fk_locomotive_depo1`
    FOREIGN KEY (`depo_id` , `depo_station_id`)
    REFERENCES `mydb`.`depo` (`id` , `station_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_locomotive_train1`
    FOREIGN KEY (`train_id`)
    REFERENCES `mydb`.`train` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`wagon`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`wagon` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `wagon_colour` VARCHAR(10) NULL,
  `wagon_size` VARCHAR(10) NOT NULL,
  `depo_id` INT NOT NULL,
  `depo_station_id` INT NOT NULL,
  `train_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_wagon_depo1_idx` (`depo_id` ASC, `depo_station_id` ASC) VISIBLE,
  INDEX `fk_wagon_train1_idx` (`train_id` ASC) VISIBLE,
  CONSTRAINT `fk_wagon_depo1`
    FOREIGN KEY (`depo_id` , `depo_station_id`)
    REFERENCES `mydb`.`depo` (`id` , `station_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_wagon_train1`
    FOREIGN KEY (`train_id`)
    REFERENCES `mydb`.`train` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`engine_driver`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`engine_driver` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `person_id` INT NOT NULL,
  `train_id` INT NOT NULL,
  `email` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`id`, `person_id`),
  INDEX `fk_engine_driver_person1_idx` (`person_id` ASC) VISIBLE,
  INDEX `fk_engine_driver_train1_idx` (`train_id` ASC) VISIBLE,
  CONSTRAINT `fk_engine_driver_person1`
    FOREIGN KEY (`person_id`)
    REFERENCES `mydb`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_engine_driver_train1`
    FOREIGN KEY (`train_id`)
    REFERENCES `mydb`.`train` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ticket_collector`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ticket_collector` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `person_id` INT NOT NULL,
  `train_id` INT NOT NULL,
  PRIMARY KEY (`id`, `person_id`),
  INDEX `fk_ticket_collector_person1_idx` (`person_id` ASC) VISIBLE,
  INDEX `fk_ticket_collector_train1_idx` (`train_id` ASC) VISIBLE,
  CONSTRAINT `fk_ticket_collector_person1`
    FOREIGN KEY (`person_id`)
    REFERENCES `mydb`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ticket_collector_train1`
    FOREIGN KEY (`train_id`)
    REFERENCES `mydb`.`train` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`working_day`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`working_day` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `day` INT NOT NULL,
  `month` INT NOT NULL,
  `year` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`train_working_day`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`train_working_day` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `working_day_id` INT NOT NULL,
  PRIMARY KEY (`id`, `working_day_id`),
  INDEX `fk_train_working_day_working_day1_idx` (`working_day_id` ASC) VISIBLE,
  CONSTRAINT `fk_train_working_day_working_day1`
    FOREIGN KEY (`working_day_id`)
    REFERENCES `mydb`.`working_day` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`train_working_day_train`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`train_working_day_train` (
  `train_working_day_id` INT NOT NULL,
  `train_working_day_working_day_id` INT NOT NULL,
  `train_id` INT NOT NULL,
  `train_controller_id` INT NOT NULL,
  `train_controller_person_id` INT NOT NULL,
  PRIMARY KEY (`train_working_day_id`, `train_working_day_working_day_id`, `train_id`),
  INDEX `fk_train_working_day_has_train_train1_idx` (`train_id` ASC) VISIBLE,
  INDEX `fk_train_working_day_has_train_train_working_day1_idx` (`train_working_day_id` ASC, `train_working_day_working_day_id` ASC) VISIBLE,
  INDEX `fk_train_working_day_train_train_controller1_idx` (`train_controller_id` ASC, `train_controller_person_id` ASC) VISIBLE,
  CONSTRAINT `fk_train_working_day_has_train_train_working_day1`
    FOREIGN KEY (`train_working_day_id` , `train_working_day_working_day_id`)
    REFERENCES `mydb`.`train_working_day` (`id` , `working_day_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_train_working_day_has_train_train1`
    FOREIGN KEY (`train_id`)
    REFERENCES `mydb`.`train` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_train_working_day_train_train_controller1`
    FOREIGN KEY (`train_controller_id` , `train_controller_person_id`)
    REFERENCES `mydb`.`train_controller` (`id` , `person_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`user` (
  `username` VARCHAR(16) NOT NULL,
  `email` VARCHAR(255) NULL,
  `password` VARCHAR(32) NOT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP);


-- -----------------------------------------------------
-- Table `mydb`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`category` (
  `category_id` INT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`category_id`));


-- -----------------------------------------------------
-- Table `mydb`.`timestamps`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`timestamps` (
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` TIMESTAMP NULL);

USE `train_transport_management` ;

-- -----------------------------------------------------
-- Table `train_transport_management`.`station`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `train_transport_management`.`station` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `station_name` VARCHAR(10) NOT NULL,
  `voltage` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mydb`.`city`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`city` (`id`, `city_name`) VALUES (1, 'Brno');
INSERT INTO `mydb`.`city` (`id`, `city_name`) VALUES (2, 'Vyškov');
INSERT INTO `mydb`.`city` (`id`, `city_name`) VALUES (3, 'Prostějov');
INSERT INTO `mydb`.`city` (`id`, `city_name`) VALUES (4, 'Olomouc');
INSERT INTO `mydb`.`city` (`id`, `city_name`) VALUES (5, 'Přerov');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`station`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`station` (`id`, `station_name`, `traffic`, `city_id`) VALUES (1, 'B_station', 'high', 1);
INSERT INTO `mydb`.`station` (`id`, `station_name`, `traffic`, `city_id`) VALUES (2, 'V_station', 'medium', 2);
INSERT INTO `mydb`.`station` (`id`, `station_name`, `traffic`, `city_id`) VALUES (3, 'PV_station', 'medium', 3);
INSERT INTO `mydb`.`station` (`id`, `station_name`, `traffic`, `city_id`) VALUES (4, 'OL_station', 'high', 4);
INSERT INTO `mydb`.`station` (`id`, `station_name`, `traffic`, `city_id`) VALUES (5, 'PŘ_station', 'medium', 5);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`depo`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`depo` (`id`, `depo_size`, `depo_name`, `station_id`, `city_id`, `station_id1`, `station_city_id`) VALUES (1, 'big', 'depoA', 1, 1, 1, 1);
INSERT INTO `mydb`.`depo` (`id`, `depo_size`, `depo_name`, `station_id`, `city_id`, `station_id1`, `station_city_id`) VALUES (2, 'medium', 'depoB', 2, 2, 2, 2);
INSERT INTO `mydb`.`depo` (`id`, `depo_size`, `depo_name`, `station_id`, `city_id`, `station_id1`, `station_city_id`) VALUES (3, 'small', 'depoC', 3, 3, 3, 3);
INSERT INTO `mydb`.`depo` (`id`, `depo_size`, `depo_name`, `station_id`, `city_id`, `station_id1`, `station_city_id`) VALUES (4, 'big', 'depoD', 4, 4, 4, 4);
INSERT INTO `mydb`.`depo` (`id`, `depo_size`, `depo_name`, `station_id`, `city_id`, `station_id1`, `station_city_id`) VALUES (5, 'medium', 'depoE', 5, 5, 5, 5);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`person`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (1, 'BROWN', 33, 'male', 10);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (2, 'CAMPBELL', 41, 'female', 15);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (3, 'SMITH', 52, 'male', 5);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (4, 'WILSON', 24, 'female', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (5, 'STEWART', 29, 'male', 5);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (6, 'ANDERSON', 25, 'female', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (7, 'SCOTT', 24, 'female', 1);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (8, 'MACDONALD', 27, 'male', 3);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (9, 'TAYLOR', 35, 'female', 4);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (10, 'ROSS', 34, 'female', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (11, 'MURRAY', 39, 'male', 9);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (12, 'REID', 54, 'male', 16);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (13, 'CLARK', 28, 'male', 4);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (14, 'YOUNG', 45, 'male', 12);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (15, 'PATERSON', 44, 'female', 4);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (16, 'WATSON', 24, 'female', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (17, 'WALKER', 39, 'male', 15);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (18, 'HENDERSON', 54, 'male', 20);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (19, 'ROBERTSON', 57, 'female', 14);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (20, 'MITCHELL', 39, 'male', 5);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (21, 'FRASER', 25, 'male', 3);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (22, 'RODRIGUEZ ', 59, 'female', 8);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (23, 'GARCIA ', 60, 'male', 24);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (24, 'GONZALEZ ', 22, 'female', 1);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (25, 'HARRIS ', 24, 'female', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (26, 'WHITE ', 28, 'male', 1);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (27, 'MARTIN ', 36, 'male', 5);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (28, 'JACKSON ', 34, 'female', 11);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (29, 'MOORE ', 39, 'female', 15);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (30, 'RAMIREZ ', 25, 'male', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (31, 'LEWIS ', 27, 'male', 5);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (32, 'ROBINSON ', 33, 'male', 9);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (33, 'WALKER ', 36, 'female', 12);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (34, 'YOUNG ', 51, 'female', 14);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (35, 'ALLEN ', 56, 'male', 18);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (36, 'KING ', 55, 'male', 17);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (37, 'WRIGHT ', 23, 'female', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (38, 'SCOTT ', 45, 'male', 15);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (39, 'TORRES ', 35, 'female', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (40, 'NGUYEN ', 44, 'male', 4);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (41, 'HILL ', 28, 'female', 5);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (42, 'FLORES ', 25, 'male', 4);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (43, 'GREEN ', 24, 'male', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (44, 'ADAMS ', 26, 'female', 4);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (45, 'NELSON ', 27, 'female', 5);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (46, 'BAKER ', 35, 'female', 3);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (47, 'HALL ', 34, 'female', 5);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (48, 'RIVERA ', 48, 'male', 7);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (49, 'CARTER ', 49, 'female', 9);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (50, 'TURNER ', 25, 'female', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (51, 'DIAZ ', 45, 'male', 17);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (52, 'CRUZ ', 31, 'female', 7);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (53, 'MORRIS ', 30, 'male', 5);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (54, 'COOK ', 26, 'female', 4);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (55, 'ROGERS ', 25, 'female', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (56, 'WEINSTEIN ', 45, 'male', 2);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (57, 'HALKER', 51, 'male', 14);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (58, 'ROKERS', 34, 'female', 7);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (59, 'DAVIES', 43, 'male', 8);
INSERT INTO `mydb`.`person` (`id`, `last_name`, `age`, `gender`, `working_years`) VALUES (60, 'KALEY', 34, 'female', 7);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`train_controller`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`train_controller` (`id`, `person_id`) VALUES (1, 59);
INSERT INTO `mydb`.`train_controller` (`id`, `person_id`) VALUES (2, 60);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`train`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`train` (`id`, `train_name`, `speed`, `station_id`, `depo_id`, `depo_station_id`, `train_id`, `train_controller_id`, `train_controller_person_id`) VALUES (1, 'R-902', 'express', 2, 2, 2, 1, 1, 59);
INSERT INTO `mydb`.`train` (`id`, `train_name`, `speed`, `station_id`, `depo_id`, `depo_station_id`, `train_id`, `train_controller_id`, `train_controller_person_id`) VALUES (2, 'R-803', 'medium', 5, 5, 5, 2, 2, 60);
INSERT INTO `mydb`.`train` (`id`, `train_name`, `speed`, `station_id`, `depo_id`, `depo_station_id`, `train_id`, `train_controller_id`, `train_controller_person_id`) VALUES (3, 'R-405', 'ultra-express', 1, 1, 1, 3, 1, 59);
INSERT INTO `mydb`.`train` (`id`, `train_name`, `speed`, `station_id`, `depo_id`, `depo_station_id`, `train_id`, `train_controller_id`, `train_controller_person_id`) VALUES (4, 'R-479', 'express', 3, 3, 3, 4, 2, 60);
INSERT INTO `mydb`.`train` (`id`, `train_name`, `speed`, `station_id`, `depo_id`, `depo_station_id`, `train_id`, `train_controller_id`, `train_controller_person_id`) VALUES (5, 'R-587', 'ultra-express', 4, 4, 4, 5, 1, 59);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`locomotive`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`locomotive` (`id`, `locomotive_colour`, `type`, `depo_id`, `depo_station_id`, `train_id`) VALUES (1, 'red', 'LT-48', 2, 2, 1);
INSERT INTO `mydb`.`locomotive` (`id`, `locomotive_colour`, `type`, `depo_id`, `depo_station_id`, `train_id`) VALUES (2, 'blue', 'LT-62', 4, 4, 5);
INSERT INTO `mydb`.`locomotive` (`id`, `locomotive_colour`, `type`, `depo_id`, `depo_station_id`, `train_id`) VALUES (3, 'yellow', 'LT-57', 1, 1, 3);
INSERT INTO `mydb`.`locomotive` (`id`, `locomotive_colour`, `type`, `depo_id`, `depo_station_id`, `train_id`) VALUES (4, 'green', 'LT-47', 3, 3, 4);
INSERT INTO `mydb`.`locomotive` (`id`, `locomotive_colour`, `type`, `depo_id`, `depo_station_id`, `train_id`) VALUES (5, 'orange', 'LT15', 5, 5, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`wagon`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (1, 'blue', 'small', 2, 2, 1);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (2, 'white', 'medium', 3, 3, 4);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (3, 'red', 'large', 2, 2, 1);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (4, 'purple', 'medium', 3, 3, 4);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (5, 'green', 'medium', 1, 1, 3);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (6, 'green', 'small', 4, 4, 5);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (7, 'red', 'large', 1, 1, 3);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (8, 'purple', 'large', 4, 4, 5);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (9, 'blue', 'medium', 5, 5, 2);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (10, 'black', 'small', 2, 2, 1);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (11, 'red', 'medium', 2, 2, 1);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (12, 'green', 'large', 2, 2, 1);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (13, 'black', 'small', 5, 5, 2);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (14, 'black', 'large', 5, 5, 2);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (15, 'purple', 'medium', 1, 1, 3);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (16, 'purple', 'small', 4, 4, 5);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (17, 'blue', 'medium', 1, 1, 3);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (18, 'green', 'small', 3, 3, 4);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (19, 'black', 'large', 5, 5, 2);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (20, 'red', 'large', 3, 3, 4);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (21, 'black', 'medium', 5, 5, 2);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (22, 'black', 'small', 4, 4, 5);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (23, 'black', 'small', 2, 2, 1);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (24, 'red', 'large', 4, 4, 5);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (25, 'blue', 'medium', 2, 2, 1);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (26, 'red', 'medium', 1, 1, 3);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (27, 'purple', 'large', 3, 3, 4);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (28, 'green', 'medium', 1, 1, 3);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (29, 'purple', 'medium', 3, 3, 4);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (30, 'green', 'large', 4, 4, 5);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (31, 'purple', 'medium', 3, 3, 4);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (32, 'black', 'large', 4, 4, 5);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (33, 'red', 'medium', 3, 3, 4);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (34, 'black', 'medium', 2, 2, 1);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (35, 'red', 'large', 1, 1, 3);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (36, 'black', 'large', 2, 2, 1);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (37, 'green', 'medium', 1, 1, 3);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (38, 'green', 'medium', 5, 5, 2);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (39, 'blue', 'small', 2, 2, 1);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (40, 'red', 'large', 5, 5, 2);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (41, 'red', 'small', 2, 2, 1);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (42, 'red', 'small', 4, 4, 5);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (43, 'black', 'small', 4, 4, 5);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (44, 'blue', 'medium', 4, 4, 5);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (45, 'purple', 'small', 5, 5, 2);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (46, 'green', 'small', 5, 5, 2);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (47, 'green', 'small', 3, 3, 4);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (48, 'purple', 'medium', 3, 3, 4);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (49, 'red', 'large', 1, 1, 3);
INSERT INTO `mydb`.`wagon` (`id`, `wagon_colour`, `wagon_size`, `depo_id`, `depo_station_id`, `train_id`) VALUES (50, 'red', 'large', 1, 1, 3);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`engine_driver`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`engine_driver` (`id`, `person_id`, `train_id`, `email`) VALUES (1, 5, 1, 'STEWART@EMAIL.COM');
INSERT INTO `mydb`.`engine_driver` (`id`, `person_id`, `train_id`, `email`) VALUES (2, 7, 1, 'SCOTT@EMAIL.COM');
INSERT INTO `mydb`.`engine_driver` (`id`, `person_id`, `train_id`, `email`) VALUES (3, 9, 2, 'TAYLOR@EMAIL.COM');
INSERT INTO `mydb`.`engine_driver` (`id`, `person_id`, `train_id`, `email`) VALUES (4, 11, 3, 'MURRAY@EMAIL.COM');
INSERT INTO `mydb`.`engine_driver` (`id`, `person_id`, `train_id`, `email`) VALUES (5, 13, 3, 'CLARK@EMAIL.COM');
INSERT INTO `mydb`.`engine_driver` (`id`, `person_id`, `train_id`, `email`) VALUES (6, 15, 4, 'PATERSON@EMAIL.COM');
INSERT INTO `mydb`.`engine_driver` (`id`, `person_id`, `train_id`, `email`) VALUES (7, 17, 4, 'WALKER@EMAIL.COM');
INSERT INTO `mydb`.`engine_driver` (`id`, `person_id`, `train_id`, `email`) VALUES (8, 19, 5, 'ROBERTSON@EMAIL.COM');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`ticket_collector`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (1, 1, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (2, 2, 2);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (3, 3, 4);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (4, 4, 5);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (5, 6, 3);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (6, 8, 2);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (7, 10, 3);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (8, 12, 4);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (9, 14, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (10, 16, 5);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (11, 18, 4);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (12, 20, 4);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (13, 21, 3);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (14, 22, 2);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (15, 23, 5);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (16, 24, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (17, 25, 4);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (18, 26, 4);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (19, 27, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (20, 28, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (21, 29, 5);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (22, 30, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (23, 31, 5);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (24, 32, 3);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (25, 33, 2);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (26, 34, 4);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (27, 35, 4);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (28, 36, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (29, 37, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (30, 38, 5);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (31, 39, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (32, 40, 5);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (33, 41, 3);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (34, 42, 3);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (35, 43, 2);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (36, 44, 2);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (37, 45, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (38, 46, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (39, 47, 4);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (40, 48, 4);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (41, 49, 2);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (42, 50, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (43, 51, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (44, 52, 2);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (45, 53, 5);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (46, 54, 4);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (47, 55, 3);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (48, 56, 2);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (49, 57, 1);
INSERT INTO `mydb`.`ticket_collector` (`id`, `person_id`, `train_id`) VALUES (50, 58, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`working_day`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`working_day` (`id`, `day`, `month`, `year`) VALUES (1, 1, 11, 2021);
INSERT INTO `mydb`.`working_day` (`id`, `day`, `month`, `year`) VALUES (2, 3, 11, 2021);
INSERT INTO `mydb`.`working_day` (`id`, `day`, `month`, `year`) VALUES (3, 5, 11, 2021);
INSERT INTO `mydb`.`working_day` (`id`, `day`, `month`, `year`) VALUES (4, 7, 11, 2021);
INSERT INTO `mydb`.`working_day` (`id`, `day`, `month`, `year`) VALUES (5, 9, 11, 2021);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`train_working_day`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`train_working_day` (`id`, `working_day_id`) VALUES (1, 1);
INSERT INTO `mydb`.`train_working_day` (`id`, `working_day_id`) VALUES (2, 2);
INSERT INTO `mydb`.`train_working_day` (`id`, `working_day_id`) VALUES (3, 3);
INSERT INTO `mydb`.`train_working_day` (`id`, `working_day_id`) VALUES (5, 4);
INSERT INTO `mydb`.`train_working_day` (`id`, `working_day_id`) VALUES (5, 5);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`train_working_day_train`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`train_working_day_train` (`train_working_day_id`, `train_working_day_working_day_id`, `train_id`, `train_controller_id`, `train_controller_person_id`) VALUES (1, 1, 2, DEFAULT, DEFAULT);
INSERT INTO `mydb`.`train_working_day_train` (`train_working_day_id`, `train_working_day_working_day_id`, `train_id`, `train_controller_id`, `train_controller_person_id`) VALUES (2, 2, 4, DEFAULT, DEFAULT);
INSERT INTO `mydb`.`train_working_day_train` (`train_working_day_id`, `train_working_day_working_day_id`, `train_id`, `train_controller_id`, `train_controller_person_id`) VALUES (3, 3, 3, DEFAULT, DEFAULT);
INSERT INTO `mydb`.`train_working_day_train` (`train_working_day_id`, `train_working_day_working_day_id`, `train_id`, `train_controller_id`, `train_controller_person_id`) VALUES (4, 4, 5, DEFAULT, DEFAULT);
INSERT INTO `mydb`.`train_working_day_train` (`train_working_day_id`, `train_working_day_working_day_id`, `train_id`, `train_controller_id`, `train_controller_person_id`) VALUES (5, 5, 1, DEFAULT, DEFAULT);

COMMIT;

