Hi!

Thanks for opening this file. This is an introduction for this project where we are making a database called Train transport management. In this repository you will find scripts for creating a database for PostgreSQL and MySQL. There is a picture representing the database design in jpg format Train_transport_management_diagram and screenshots of already created database in PostgreSQL (from the pgAdmin). There is also an application description with all tables included.
The database was made for train transport management with tables of trains, wagons, locomotives, stations, cities and people included. 

Hope you will find what you need.
Thanks for reading.
